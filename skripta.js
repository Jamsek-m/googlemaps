var mx = 0;
var my = 0;

function izpis(){
	var file = "bolnice.json";
	readTextFile(file);
}

function readTextFile(file){
	var rawFile = new XMLHttpRequest();
	rawFile.open("GET", file, false);
	rawFile.onreadystatechange = function ()
	{
		if(rawFile.readyState === 4)
		{
			if(rawFile.status === 200 || rawFile.status == 0)
			{
				var allText = rawFile.responseText;
				var array = allText.split("\n");
				var out = document.getElementById("out");
				var pravi = document.getElementById('pravi');
				var obj = JSON.parse(allText);

				for(var i = 0; i < obj.bolnice.length; i++){
					var presledek = (i == 0)? "":"<hr>";
					pravi.innerHTML += presledek+""+obj.bolnice[i].ime+"<br>"+obj.bolnice[i].naslov+"<br>"+obj.bolnice[i].kraj+"<br>";
					pravi.innerHTML += "lat: "+obj.bolnice[i].lat+", lng: "+obj.bolnice[i].lng+"<br>";
				}

				mx = parseFloat(obj.bolnice[0].lat);
				my = parseFloat(obj.bolnice[0].lng);
				initMap();
			}
		}
	}
	rawFile.send(null);
}

var openFile = function(event) {
		var input = event.target;

		var reader = new FileReader();
		reader.onload = function(){
		  var text = reader.result;
		  var node = document.getElementById('output');
		  node.innerText = text;
		  console.log(reader.result.substring(0, 200));
		};
		reader.readAsText(input.files[0]);
};

function getLocation() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition);
	}
}

function showPosition(position) {
	var x = position.coords.latitude;
	var y = position.coords.longitude;

}

function initialize(){
  var mapProp = {
    center: new google.maps.LatLng(51.508742,-0.120850),
    zoom:7,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };
  var map = new google.maps.Map(document.getElementById("map"),mapProp);
}

function loadScript(){
	var script = document.createElement("script");
	script.src = "https://maps.googleapis.com/maps/api/js?key=AIzaSyA8y0ST4tDKXRRMvGqSAFv4kfQ41tfEl7A&sensor=false&callback=initialize";
	document.body.appendChild(script);
}

function initMap() {
	console.log("LOG: "+mx+", "+my);
	var location = new google.maps.LatLng(mx,my);
	var map = new google.maps.Map(document.getElementById('map'), {
		zoom: 16,
		center: {lat:mx,lng:my}
	});
}